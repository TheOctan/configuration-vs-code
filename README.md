# My favorites Extensions

# By default
## [Russian Language Pack for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=MS-CEINTL.vscode-language-pack-ru)
## [Favorites](https://marketplace.visualstudio.com/items?itemName=kdcro101.favorites)
## [TODO Highlight](https://marketplace.visualstudio.com/items?itemName=wayou.vscode-todo-highlight)
## [Live Share](https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare)
## [Bookmarks](https://marketplace.visualstudio.com/items?itemName=alefragnani.Bookmarks)
## [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

# Themes
## [Material Icon Theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)
## [Material Theme](https://marketplace.visualstudio.com/items?itemName=Equinusocio.vsc-material-theme)
## [Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)

# Termnial
## [Shell launcher](https://marketplace.visualstudio.com/items?itemName=Tyriar.shell-launcher)
## [PowerShell](https://marketplace.visualstudio.com/items?itemName=ms-vscode.PowerShell)

# Git
## [Git Extension Pack](https://marketplace.visualstudio.com/items?itemName=donjayamanne.git-extension-pack)
> ## [Git History](https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory)
> ## [Project Manager](https://marketplace.visualstudio.com/items?itemName=alefragnani.project-manager)
> ## [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
> ## [gitignore](https://marketplace.visualstudio.com/items?itemName=codezombiech.gitignore)
> ## [Open in GitHub, Bitbucket, Gitlab, VisualStudio](https://marketplace.visualstudio.com/items?itemName=ziyasal.vscode-open-in-github)
## [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph)
## [GitHub Pull Requests](https://marketplace.visualstudio.com/items?itemName=GitHub.vscode-pull-request-github)
## [Git Project Manager](https://marketplace.visualstudio.com/items?itemName=felipecaputo.git-project-manager)
## [Gist](https://marketplace.visualstudio.com/items?itemName=kenhowardpdx.vscode-gist)
## [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)

# С++
## [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)
## [C++ Helper](https://marketplace.visualstudio.com/items?itemName=amiralizadeh9480.cpp-helper)
## [C++ Intellisence](https://marketplace.visualstudio.com/items?itemName=austin.code-gnu-global)

# Unity
## [unity3d-pack](https://marketplace.visualstudio.com/items?itemName=dimasalmaz.unity3d-pack)
> ## [C#](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp)
> ## [C# FixFormat](https://marketplace.visualstudio.com/items?itemName=Leopotam.csharpfixformat)
> ## [C# Snippets](https://marketplace.visualstudio.com/items?itemName=jorgeserrano.vscode-csharp-snippets)
> ## [C# XML Documentation Comments](https://marketplace.visualstudio.com/items?itemName=k--kato.docomment)
> ## [Debugger for Unity](https://marketplace.visualstudio.com/items?itemName=Unity.unity-debug)
> ## [Shader languages support for VS Code](https://marketplace.visualstudio.com/items?itemName=slevesque.shader)
> ## [Unity Code Snippets](https://marketplace.visualstudio.com/items?itemName=kleber-swf.unity-code-snippets)
> ## [Unity Tools](https://marketplace.visualstudio.com/items?itemName=Tobiah.unity-tools)
> ## [ShaderlabVSCode(Free)](https://marketplace.visualstudio.com/items?itemName=amlovey.shaderlabvscodefree)
> ## Code Outline
## [C# Extensions](https://marketplace.visualstudio.com/items?itemName=jchannon.csharpextensions)
## [Unity Snippets](https://marketplace.visualstudio.com/items?itemName=YclepticStudios.unity-snippets)

# Java
## [Java Extension Pack](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack)
> ## [Language Support for Java(TM)](https://marketplace.visualstudio.com/items?itemName=redhat.java)
> ## [Debugger for Java](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-debug)
> ## [Java Test Runner](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-test)
> ## [Maven for Java](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-maven)
> ## [Java Dependency Viewer](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-dependency)
> ## [Visual Studio IntelliCode](https://marketplace.visualstudio.com/items?itemName=VisualStudioExptTeam.vscodeintellicode)
## [Java Linter](https://marketplace.visualstudio.com/items?itemName=faustinoaq.javac-linter)
## [Java Code Generators](https://marketplace.visualstudio.com/items?itemName=sohibe.java-generate-setters-getters)

<!-- # Android
## Android
## Android iOS Emulator
## Dart
## Flutter
## Kotlin -->

# HTML & CSS
## [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
## [HTML CSS Support](https://marketplace.visualstudio.com/items?itemName=ecmel.vscode-html-css)
## [HTML Snippets](https://marketplace.visualstudio.com/items?itemName=abusaidm.html-snippets)
## [HTML Boilerplate](https://marketplace.visualstudio.com/items?itemName=sidthesloth.html5-boilerplate)
## [HTML Preview](https://marketplace.visualstudio.com/items?itemName=tht13.html-preview-vscode)
## [Picture Element](https://marketplace.visualstudio.com/items?itemName=JakeWilson.vscode-picture)
## [Auto Rename Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)
## [IntelliSense for CSS class names in HTML](https://marketplace.visualstudio.com/items?itemName=Zignd.html-css-class-completion)
## [CSS Peek](https://marketplace.visualstudio.com/items?itemName=pranaygp.vscode-css-peek)
## [Color Highlight](https://marketplace.visualstudio.com/items?itemName=naumovs.color-highlight)
## [XML](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-xml)
## [XML Tools](https://marketplace.visualstudio.com/items?itemName=DotJoshJohnson.xml)

# Java Script
## [JavaScript (ES6)](https://marketplace.visualstudio.com/items?itemName=xabikos.JavaScriptSnippets)
## [Debugger for Chrome](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome)
## [Turbo Console Log](https://marketplace.visualstudio.com/items?itemName=ChakrounAnas.turbo-console-log)
## [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
## [Document This](https://marketplace.visualstudio.com/items?itemName=joelday.docthis)
## [Prettier Now](https://marketplace.visualstudio.com/items?itemName=remimarsal.prettier-now)
## [Visual Studio IntelliCode](https://marketplace.visualstudio.com/items?itemName=VisualStudioExptTeam.vscodeintellicode)

# PHP
## [PHP Debug](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug)
## [PHP IntelliSense](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-intellisense)
## [phpcs](https://marketplace.visualstudio.com/items?itemName=ikappas.phpcs)
## [PHP Server](https://marketplace.visualstudio.com/items?itemName=brapifra.phpserver)

# Lua
## [Lua language support for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=keyring.Lua)
## [Lua Language Server coded by Lua](https://marketplace.visualstudio.com/items?itemName=sumneko.lua)
## [Lua Debug](https://marketplace.visualstudio.com/items?itemName=actboy168.lua-debug)
## [Lua lint](https://marketplace.visualstudio.com/items?itemName=satoren.lualint)

# Shaders
## [ShaderlabVSCode(Free)](https://marketplace.visualstudio.com/items?itemName=amlovey.shaderlabvscodefree)
## [GLSL Lint](https://marketplace.visualstudio.com/items?itemName=CADENAS.vscode-glsllint)
## [GLSL Linter](https://marketplace.visualstudio.com/items?itemName=mrjjot.vscode-glsl-linter)
## [glsl-canvas](https://marketplace.visualstudio.com/items?itemName=circledev.glsl-canvas)

# CMake
## [CMake langage support for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=twxs.cmake)
## [CMake Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools)
## [CMake Test Explorer](https://marketplace.visualstudio.com/items?itemName=fredericbonnet.cmake-test-adapter)
## [cmake-format](https://marketplace.visualstudio.com/items?itemName=cheshirekow.cmake-format)

# Python
## [Python Extension Pack](https://marketplace.visualstudio.com/items?itemName=donjayamanne.python-extension-pack)
